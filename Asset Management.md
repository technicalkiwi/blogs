---
slug: asset_management
title: Asset Management
authors: tkiwi
tags: [application, asset-management, enterprise]
---
# Introduction
This week I tasked myself to setup a new Asset management system, we are currently on the outdated CMDBUILD platform

<!--truncate-->
### Requirements

First step was to write out the requirements for the new system
- Able to handle a large amount of data
- Open Source
- Ability to allocate assets to users and assign licenses to people or assets
- Easy to import existing data
- Able to update records via CSV import
- Have a small footprint
- Easy to Manage access levels

 
### Descision Time

After researching the various offerings I settled on [Snipe-IT](https://snipeitapp.com/)
 

# Setup
## Server Setup
I will be using Ubunutu for the base OS as this will keep it consistent with our other internal sites
According to the Snipe-IT documentation this system works best when built on top of an Apache webserver so this will be done on top of a LAMP Stack
We will skip over the [Ubuntu Install](https://linuxconfig.org/how-to-install-ubuntu-18-04-bionic-beaver) install as there are a million guides available online
I also installed the rest of the LAMP stack with a prewritten [script](https://github.com/temightyspoon/Bash-Scripts/blob/master/LAMP_install.sh) of mine
I then installed the site itself following this [guide here](https://www.tecmint.com/install-snipe-it-asset-management-on-centos-ubuntu-debian/) with some modifications of course ;)


## Application Setup

1. First step was to install all the needed php modules
`sudo apt install php-(pdo mbstring tokenizer curl mysql ldap zip fileinfo gd dom)`

2. Second step was to run through the DB secure process
Making sure to set a strong root password
`sudo mysql_secure_installation`
(picture)

3. Once that was complete I moved onto creating a user and the database for snipe-it to store the data in.
(picture)

4. I then moved onto installing composer
`cd /tmp`
`sudo curl -sS https://getcomposer.org/installer | php`
`sudo mv composer.phar /usr/local/bin/composer`

5. then it was time to copy the application from the github repository
`cd /var/www`
`mkdir snipe-it`
`sudo git clone https://github.com/snipe/snipe-it.git snipe-it`

6. Once the download has finished move into the directory and copy the .env.example file into .env
`cd /var/www/snipeit`
`sudo cp .env.example .env`

7. Edit the .env file
`sudo nano .env`

8. Add in or alter the below conifguration
``` bash
APP_TIMEZONE= #Change it according to your country
APP_URL= #Set your domain name or IP address
DB_HOST= #Set it to localhost
DB_DATABASE= #Set the database name
DB_USERNAME= #Set the database username
DB_PASSWORD= #Set the database user password
```

9. Then took over the folders from where the data would be served
These are all in the snipe-it folder /var/www/snipe-it
`sudo chmod -R 755 storage`
`sudo chmod -R 755 public/uploads`
`sudo chown -R www-data:www-data storage public/uploads`

(picture)

10. Run composer in the snipe-it directory
`sudo composer install --no-dev –prefer-source`

11.  Generate your app key value (this will set automatically in the .env file)
`sudo php artisan key:generate`

12. Now move onto creating the virtual host file
`sudo nano /etc/apache2/sites-available/snipeit.conf`

13. Add in the below server block.
``` shell
VirtualHost #IP Here&gt;
    ServerName #Your Domain or IP Here
    DocumentRoot /var/www/snipe-it/public
    Directory /var/www/snipe-it/public;
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    /Directory;
/VirtualHost;
``````

Once that was all up and running I ran through the initial setup of snipe it
Set an Admin user name and password, checked it connected to the DB, etc

(Picture)

Finished our new Asset Management platform is up and running
(Picture)
Now on to transferring the data across.

## Data Setup
The current Asset system supports exporting to CSV so that was step one
I then had to manipulate it to match the new platforms import fields
Import the file to snipe it
(picture)
Luckily Snipe-it supports custom field mappings so it didn’t have to be exact. 
(Picture)