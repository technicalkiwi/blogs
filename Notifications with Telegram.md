---
slug: login-notifications-telegram
title: Login Notifications with Telegram
authors: tkiwi
tags: [telegram, notifications, ssh]
---

# Introduction
.

### Requirements
- Push Notifications
- Central Source for all Servers
- Notification History
- Accessible from Multiple Sources

<!--truncate-->

# Setup
### Telegram Setup
Create Telegram account
Login to new account
### Bot setup
Start chat with BotFather
Create a new bot `/new bot`
Fill in the bots name, (must end with bot)
The Botfather will reply with the bots API token
Take note of this as it is vital
Go back and start a chat with your bot
### Command setup
Go to `https://api.telegram.org/bot$TOKEN/getUpdates`
Where $ Token is the API key you received from Botfather
You should get something similar to the below

## Server Setup